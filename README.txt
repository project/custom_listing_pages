7.x-1.x RELEASE NOTE
 (Sept 9, 2012) Initial release.



The Custom Listing Pages module provides the ability to create custom pages 
with data from existing content types. 
 - Display data from an existing content type with various options including:
   - View Mode to display.
   - Filter by one or more taxonomy vocabularies linked to the content type 
selected.
   - Filter further by the tags for each selected taxonomy vocabularies.
   - Sort by (title, last updated date, created date)
   - Sort order (ascending, descending)
   - Entries to display per page (pagination)


The Custom Listing Pages was sponsored by the University of Waterloo 
(uwaterloo.ca)


Dependencies
------------
 * select_or_other

Requirements
------------
 PHP 5.2+ 


Use
-------
- Navigate to "Add Content" in your drupal administration panel. (node/add)
- Select "Custom Listing Pages Base" from the list.
- Manage what content is displayed from which content type
