<?php

/**
 * @file
 * custom_listing_pages.features.inc
 * Support for features export
 */

/**
 * Constant definition for data handle
 */
define("CUSTOM_LISTING_PAGES_DATA", "custom_listing_pages_data");

/**
 * Implements hook_features_export_options().
 */
function custom_listing_pages_data_features_export_options() {
  $options = array();
  $options[CUSTOM_LISTING_PAGES_DATA] = "Custom Listing Page configuration data";
  return $options;
}

/**
 * Implements hook_features_export().
 */
function custom_listing_pages_data_features_export($data, &$export, $module_name) {
  /*
   * We have module dependencies in order for this module to function properly 
   * so we will add them here.
   */
  $export['dependencies']['custom_listing_pages'] = 'custom_listing_pages';

  /*
   * The following is the simplest implementation of a straight object export
   * with no further export processors called.
   */
  foreach ($data as $component) {
    $export['features']['custom_listing_pages_data'][$component] = $component;
  }
  return array();
}

/**
 * Returns custom listing pages data from the database as an array for export.
 */
function _custom_listing_pages_data_get_data($sys_name) {
  if ($sys_name == CUSTOM_LISTING_PAGES_DATA) {
    // Get db data.
    $data = array();
    $result = db_query("SELECT view_mode, content_type, vocabulary_matches, data_sort, entries_per_page FROM {custom_listing_pages_data} ");

    if ($result !== FALSE) {
      $row = $result->fetchAssoc();
      while ($row !== FALSE) {
        $data[] = $row;
        $row = $result->fetchAssoc();
      }

      return $data;
    }
  }
}

/**
 * Writes custom listing pages data into the database from the export array.
 */
function _custom_listing_pages_data_set_data($data) {
  // Loop over the data.
  if (is_array($data)) {
    foreach ($data as $row) {
      db_insert("custom_listing_pages_data")
      ->fields(array(
          'view_mode' => $row['view_mode'],
          'content_type' => $row['content_type'],
          'vocabulary_matches' => $row['vocabulary_matches'],
          'data_sort' => $row['data_sort'],
          'entries_per_page' => $row['entries_per_page'],
      ))
      ->execute();
    }
  }
}

/**
 * Implements hook_features_export_render().
 */
function custom_listing_pages_data_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $custom_listing_page_data = array();';
  $code[] = '';

  foreach ($data as $sys_name) {
    // Retrieve the contest information.
    $item = _custom_listing_pages_data_get_data($sys_name);
    $code[] = '  $custom_listing_page_data[] = ' . features_var_export($item, '  ') . ';';
  }

  $code[] = '  return $custom_listing_page_data;';
  $code = implode("\n", $code);
  return array('custom_listing_pages_data_dump' => $code);
}

/**
 * Implements hook_features_rebuild(). 
 */
function custom_listing_pages_data_features_rebuild($module) {
  $items = module_invoke($module, 'custom_listing_pages_data_dump');

  // Loop over the items we need to recreate.
  foreach ($items as $idx => $item) {
    // Basic function to take the array and store in the database.
    _custom_listing_pages_data_set_data($item);
  }
}

/**
 * Implements hook_features_revert().
 */
function custom_listing_pages_data_features_revert($module) {
  custom_listing_pages_data_features_rebuild($module);
}
